package com.twuc.webApp.domain.composite;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class CompanyProfile {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 128)
    private String city;
    @Column(nullable = false, length = 128)
    private String street;

    public CompanyProfile() {
    }

    public CompanyProfile(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
