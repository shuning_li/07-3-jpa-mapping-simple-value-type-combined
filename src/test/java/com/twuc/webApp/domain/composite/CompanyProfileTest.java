package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CompanyProfileTest extends JpaTestBase {

    @Autowired
    CompanyProfileRepository repo;

    @Test
    void should_save_entity() {
        flushAndClear(em -> {
            CompanyProfile companyProfile = new CompanyProfile("shenzhen", "xxxx");
            CompanyProfile savedCompanyProfile = repo.save(companyProfile);
            assertNotNull(savedCompanyProfile);
        });
    }
}
