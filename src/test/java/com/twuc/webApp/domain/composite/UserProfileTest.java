package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserProfileTest extends JpaTestBase {

    @Autowired
    UserProfileRepository repo;

    @Test
    void should_save_entity() {
        flushAndClear(em -> {
            UserProfile userProfile = new UserProfile("shenzhen", "xxxx");
            UserProfile savedUserProfile = repo.save(userProfile);
            assertNotNull(savedUserProfile);
        });
    }
}
